#!/bin/bash

INPUT="numbers.txt"

fail() {
    printf "$*\n"
    printf "\033[0;31mEchec\n"
    exit 1
}

if [ ! -f "$INPUT" ]; then
    fail "Le fichier $INPUT n'existe pas."
fi

sort -c -n $INPUT || fail "Les nombres ne sont pas correctement triés"

FIRST=`head -1 $INPUT`
LAST=`tail -1 $INPUT`
EXPECTED_COUNT=$((LAST-FIRST+1))
ACTUAL_COUNT=`cat $INPUT | wc -l | sed "s/ //g"`
if [[ $EXPECTED_COUNT != $ACTUAL_COUNT ]]; then
  fail "Tous les nombres de $FIRST à $LAST ne sont pas présents"
fi

printf "\033[0;32mSuccès\n"
