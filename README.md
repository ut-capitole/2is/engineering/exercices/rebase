# Rebasing exercice

Update current repository :

```mermaid
gitGraph TB:
    commit id: "feat: count to 5" tag: "v1"
    branch bug-fix
    commit id: "fix: bad numbers order"
    checkout main
    commit id: "feat: count to 10"
    branch feat-count-to-20
    commit id: "feat: count to 20"
    checkout main
    commit id: "feat: start counting from 0" tag: "v2"
    checkout feat-count-to-20
    commit id: "oops"
```

To :

```mermaid
gitGraph TB:
    commit id: "feat: count to 5" tag: "v1"
    commit id: "feat: count to 10"
    commit id: "feat: start counting from 0" tag: "v2"
    commit id: "fix: bad numbers order"
    branch feat-count-to-20
    commit id: "feat: count to 20"
```

Tests must be OK (green) both on `main` and `feat-count-to-20` .
You can run them with the command

```console
$ ./runTests.sh
```
